# PICO CSS

Pico CSS is a very small CSS Framework. I use the license NPOSL, if you want to use it in a commercial project contact me.

# How to

- run :`make run`
- build (to get an executable) : `make install`
- minify css : `make css`
