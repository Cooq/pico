install: css-usage
	export KEMAL_ENV=production && crystal build src/routes.cr --release -o to_run

run: css-usage
	crystal run src/routes.cr

css-usage:
	scss src/scss/index.scss public/css/pico.css --style compressed --sourcemap=none

css:
	scss src/scss/index.scss pico.min.css --style compressed --sourcemap=none

